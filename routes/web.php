<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
 */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cockpit', 'Admin\CockpitController@index')->name('cockpit');
Route::resource('/cockpit/posts', 'Admin\PostController');
Route::resource('/cockpit/pages', 'Admin\PageController');
Route::resource('/cockpit/categories', 'Admin\CategoryController');
Route::resource('/cockpit/tags', 'Admin\TagController');
Route::resource('/cockpit/menus', 'Admin\MenuController');
Route::resource('/cockpit/reportages', 'Admin\ReportageController');

Route::get('/','PagesController@accueil')->name('accueil');

/*Route::get('/', function () {
    return view('welcome');
});*/
