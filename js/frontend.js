
$('#acceptationCookies').on('click', function(e){
    e.preventDefault();

    var url = $('#route_for_register_rgpd_cookies').html();
    $.ajax({
        url: url,
        type: 'POST',
        data: {

        },
        dataType: 'JSON',
        success: function (data) {
            $('#cookie-banner').hide();
        },
        error: function (e) {/*
            $('#forgetPasswordModal').hide();
            $('#modal-infos-body').html("Une erreur est survenue lors de l'envoi du mail de réinitialisation. Code erreur: " + e.responseText);
            $('#modal-infos').modal('show');
            // console.log(e.responseText);*/
        }
    });
});

$('#backtotop').on('click', function(){
    $('html,body').animate({scrollTop:0},1000);
});
$(window).scroll(function() {
    if ($(window).scrollTop() > 10) {
        $('#backtotop').show();
    } else {
        $('#backtotop').hide();
    }
});