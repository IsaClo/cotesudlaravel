if (typeof $('.select2').val() != "undefined"){
    $('.select2').select2();
}


if (typeof $('#autotags').val() != "undefined") {
    $tokenField = $('#autotags');
    $tokenField.tokenfield({
        autocomplete: {
            //source: ['red', 'bkue'],
            source: $tokenField.data('url'),
            minLength: 2,
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
}