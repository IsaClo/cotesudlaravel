@if($action=='store')
    <form action="{{ action('Admin\PostController@store') }}" method="POST">

        @else
            <form action="{{ action('Admin\PostController@update', $post) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                @endif

                {{ csrf_field() }}

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Tags</label>
                    <div class="col-sm-10">
                        <select name="tag_id" id="" class="select2" style="width: 100%">
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Titre</label>
                    <div class="col-sm-10">
                        <input type="text"  class="form-control" name="title" value="{{ old('title',$post->title) }}">
                    </div>
                </div>
                <div class="form-group row " style="display: none">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Url</label>
                    <div class="col-sm-10">
                        <input type="text"  class="form-control" name="slug">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label" >Contenu</label>
                    <div class="col-sm-10">
                        <textarea class="editor" name="content" cols="30" rows="10">{{ old('content',$post->content) }}</textarea>
                    </div>
                </div>

                <button type="submit">Enregistrer</button>
            </form>