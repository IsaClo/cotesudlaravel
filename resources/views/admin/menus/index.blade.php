@extends ('layouts.cockpit')

@section('content')
    <div class="row">
        <div class="col">
            <a class="btn btn-primary" href="{{ route('posts.create') }}">Ajouter</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table">
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>
                            {{ $post->title }}
                        </td>
                        <td>
                            {{ $post->slug }}
                        </td>
                        <td>{{ $post->updated_at }}</td>
                        <td>
                            <a href="{{ route('posts.edit', $post) }}"  title="Editer l'article" class="blue>"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{ action('Admin\PostController@destroy', $post) }}" class="red" data-method="delete" data-confirm="Voulez-vous bien supprimer cet article"><i class="fas fa-trash-alt"></i></a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
