@extends ('layouts.cockpit')

@section('content')
    <div class="row">
        <div class="col">
            <a class="btn btn-primary" href="{{ route('reportages.create') }}">Ajouter</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table">
                <tbody>
                @foreach($reportages as $reportage)
                    <tr>
                        <td>
                            {{ $reportage->title }}
                        </td>
                        <td>
                            {{ $reportage->slug }}
                        </td>
                        <td>{{ $reportage->updated_at }}</td>
                        <td>
                            <a href="{{ route('reportages.edit', $reportage) }}"  title="Editer l'article" class="blue>"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{ action('Admin\ReportageController@destroy', $reportage) }}" class="red" data-method="delete" data-confirm="Voulez-vous bien supprimer cet article"><i class="fas fa-trash-alt"></i></a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
