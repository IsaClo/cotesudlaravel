@if($action=='store')
    <form action="{{ action('Admin\ReportageController@store') }}" method="POST">

        @else
            <form action="{{ action('Admin\ReportageController@update', $reportage) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                @endif

                {{ csrf_field() }}


                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Titre</label>
                    <div class="col-sm-10">
                        <input type="text"  class="form-control" name="title" value="{{ old('title',$reportage->title) }}">
                    </div>
                </div>
                <div class="form-group row " style="display: none">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Url</label>
                    <div class="col-sm-10">
                        <input type="text"  class="form-control" name="slug">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label" >Contenu</label>
                    <div class="col-sm-10">
                        <textarea class="editor" name="content" cols="30" rows="10">{{ old('content',$reportage->content) }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Tags</label>
                    <div class="col-sm-10">
                        <input data-url="{{ route('tags.auto') }}" id="autotags" type="text" name="tags">
                    </div>
                </div>

                <button type="submit">Enregistrer</button>
            </form>
