@extends ('layouts.cockpit')

@section('content')
    <div class="row">
        <div class="col text-right">
            <a class="btn btn-warning" href="{{ route('posts.index') }}">Retour</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @include('admin.posts.form',['action' => 'store'])
        </div>
    </div>
@endsection


@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=40cakvroazrt9qmtcvc4jhwddimpi2cj26v8c03jxkfbc499"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{ asset('/js/editor.js') }}"></script>
@endsection

