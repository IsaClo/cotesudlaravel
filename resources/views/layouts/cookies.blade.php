
<div id="cookie-banner" class="cookie-banner container-fluid">
    <div class="row">
    <div class="col-12 col-lg-10">
        <p>En poursuivant votre navigation sur le site, vous acceptez l'utilisation de cookies (utilisés à des fins statistiques) tels que définis par notre politique de confidentialité.<br></p>
        </div>
            <div class="col-12 col-lg-2">
            <button id="acceptationCookies" class="btn btn-secondary" >Accepter</button>
        </div>
    </div>

</div>