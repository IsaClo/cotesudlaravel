<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @yield('seo')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->

    <link href='{{ asset('css/app.css') }}' rel="stylesheet">
    <link rel="stylesheet" href='https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css'>
    <link href='{{ asset('css/admin.css') }}' rel="stylesheet">
    @yield('css')

    <title>Coté Sud Photo</title>
    <script src="https://kit.fontawesome.com/8c23195d3a.js" crossorigin="anonymous"></script>
  </head>

  <body>
  @include('layouts.navadmin')
    <div class='container-fluid'>
    @include('layouts.flash')
    @yield('content')
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="http://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{ asset('js/laravel.js') }}" ></script>
    <script src="{{ asset('js/jqueryui.js') }}" ></script>

    @yield('js')
    <script src="{{ asset('js/admin.js') }}" ></script>
  </body>
</html>
