<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:site_name" content="Ensemble Ici"/>
@yield('seo')
<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    {{--    <link href="{{ asset('css/googlefont.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href='https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css'>
    <link href="{{ asset('css/frontend.css') }}?t=<?= time() ?>" rel="stylesheet">

    {{--    <link rel="icon" type="image/png" href="{{ url('storage/app/public/dyg_logo.png') }}" />--}}
    @yield('css')
    <script src="https://kit.fontawesome.com/8c23195d3a.js" crossorigin="anonymous"></script>
    @yield('headjs')
    {{--    TODO gestion google anaylicis par territoire  --}}
    {{--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53637163-3"></script>--}}
    {{--    <script>--}}
    {{--        window.dataLayer = window.dataLayer || [];--}}
    {{--        function gtag(){dataLayer.push(arguments);}--}}
    {{--        gtag('js', new Date());--}}

    {{--        gtag('config', 'UA-53637163-3');--}}
    {{--    </script>--}}
</head>
<body>
@include('layouts.header')
<div class="container">

    <div style="margin-top: 100px;">
        @include('layouts.flash')
        @yield('content')

        <footer>
            @include('layouts.footer')
        </footer>
        @include('layouts.modal')
        @yield('modal')
    </div>
    @include('layouts.backtotop')
    @if($cookie_rgpd == 0)
        @include('layouts.cookies')

    @endif
    <span id="route_for_register_rgpd_cookies" style="display: none">{{ route('ajax.acceptationCookies') }}</span>
</div>
{{--@include('layouts.routes')--}}
<script src="{{ asset('js/jquery341.js') }}"></script>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap441.js') }}"></script>
@yield('js')
<script src="{{ asset('js/jqueryui.js') }}"></script>
<script src="{{ asset('js/frontend.js') }}?t=<?= time() ?>"></script>
</body>
</html>
