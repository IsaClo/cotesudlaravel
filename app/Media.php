<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Behaviour\Sluggable;
use App\Behaviour\Datetime;

class Media extends Model
{
    protected $guarded = [];

    public function mediable()
    {
        return $this->morphTo();
    }
    use Sluggable;
    use Datetime;
}