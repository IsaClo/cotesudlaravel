<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Behaviour\Sluggable;
use App\Behaviour\Datetime;

class Meta extends Model
{
    protected $guarded = [];

    public function metable(){
        return $this->morphTo();
}

    use Sluggable;
    use Datetime;
}