<?php


namespace App\Behaviour;


use Illuminate\Support\Str;

trait Sluggable
{
    public function setSlugAttribute($slug) {
        $this->attributes['slug'] = Str::slug($this->title);
    }
}