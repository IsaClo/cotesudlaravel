<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Reportage;
use App\Tag;
use Illuminate\Http\Request;

class ReportageController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reportages = Reportage::orderBy('title')->get();
        return view('admin.reportages.index', ['reportages' => $reportages]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $reportage = new Reportage();
        $tags = Tag::all();
        return view('admin.reportages.create',['reportage' => $reportage, 'tags' => $tags]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $reportage =  Reportage::create($request->only(['title', 'slug', 'content']));
        $reportage->saveTags($request->tags);

        return redirect()->route('reportages.index')->with('success', "Le reportage a bien été ajouté");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reportage = Reportage::findOrFail($id);
        $tags = Tag::all();
        return view('admin.reportages.edit', ['reportage' => $reportage, 'tags' => $tags]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reportage = Reportage::findOrFail($id);
        $reportage->update($request->all());
        return redirect()->route('reportgaes.index')->with('success', "Le reportage a bien été modifié");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reportage = Reportage::findOrFail($id);
        $reportage->delete();
        return redirect()->route('reportages.index')->with('success', "Le reportage a bien été supprimé");

    }
}
