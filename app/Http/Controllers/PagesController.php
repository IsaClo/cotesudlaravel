<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function accueil(){
        return view('pages.accueil');
    }

    public function acceptationCookies(){
        $rgpd_cookie = cookie('cs_rgpd', 1, 1440*365);
        $reponse = array('code'=>'0');
        return response(json_encode($reponse))->cookie($rgpd_cookie);
    }
}
