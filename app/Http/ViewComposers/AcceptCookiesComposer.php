<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;

class AcceptCookiesComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $has_accept_cookies = 0;
        if (isset($_COOKIE['cs_rgpd'])) {
            $has_accept_cookies = 1;
        }
        $view->with('cookie_rgpd', $has_accept_cookies);
    }
}