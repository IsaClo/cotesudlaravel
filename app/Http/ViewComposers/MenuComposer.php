<?php
namespace App\Http\ViewComposers;

use App\Menu;
use App\Page;
use Illuminate\View\View;

class MenuComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $tabmenu=[];
        $menus = Menu::orderBy('parent')->orderBy('level')->orderBy('order')->get();
        foreach ($menus as $menu){
            if ($menu-> type == 1){
                $page = Page::where('id', $menu->page_id)->first();
                $menu->link = env('APP_URL').'/'.$page->slug;
            }
            $tabmenu[$menu->parent][] = $menu;
        }

        $view->with('menus', $tabmenu);
    }
}