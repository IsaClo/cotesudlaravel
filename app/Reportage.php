<?php

namespace App;

use App\Concern\Taggable;
use Illuminate\Database\Eloquent\Model;
use App\Behaviour\Sluggable;

class Reportage extends Model
{
    protected $guarded = [];

    use Taggable;

    public function metas(){
        return $this->morphMany(Meta::class, 'metable');
    }

    public function medias(){
        return $this->morphMany(Media::class, 'mediable');
    }

    use Sluggable;
}

