<?php


namespace App\Concern;


use App\Tag;
use Illuminate\Support\Str;

trait Taggable
{

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }


    public function saveTags($tags) {
        // je récupère les tags de l'article
        $tags = array_filter(array_unique(array_map(function($item) {
            return trim($item);
        }, explode(',', $tags))), function($item){
            return !empty($item);
        });


        // je récupère les tags déjà en base de données et faisant partie des tags de l'article
        $persisted_tags = Tag::whereIn('title', $tags)->get();

        // je trouve les nouveaux tags à insérer en base
        $tags_to_insert = array_diff($tags, $persisted_tags->pluck('title')->all());
        $tags_to_create = [];
        foreach ($tags_to_insert as $tag) {
            $tags_to_create[] = [
                'title' => $tag,
                'slug' => Str::slug($tag)];
        }
        $created_tags = $this->tags()->createMany($tags_to_create);
        $persisted_tags = $persisted_tags->merge($created_tags);
        $edits = $this->tags()->sync($persisted_tags);
//        Tag::whereIn('id', $edits['attached'])->increment('post_count', 1);
//        Tag::whereIn('id', $edits['detached'])->decrement('post_count', 1);
        //Tag::removeUnused();
    }
}