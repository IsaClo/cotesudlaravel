<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Behaviour\Sluggable;
use App\Behaviour\Datetime;

class Page extends Model
{
    protected $guarded = [];

    public function metas(){
        return $this->morphMany(Meta::class, 'metable');
    }

    use Sluggable;
    use Datetime;
}

